class AddPhonetizedColumnsTitleDescriptionToProducts < ActiveRecord::Migration
  def self.up
    change_table :products do |t|
      t.string :title_phonetized
      t.text   :description_phonetized
    end
  end

  def self.down
    remove_column :products, :title
    remove_column :products, :description
  end
end
