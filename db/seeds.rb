#-*- encoding: utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# Environment variables (ENV['...']) are set in the file config/application.yml.
# See http://railsapps.github.io/rails-environment-variables.html

Product.delete_all
Product.create([
                {title: 'ÁCIDO ACÉTICO GLACIAL 99%',                description: 'ácido acético glacial 99%'},
                {title: 'ÁCIDO ACRÍLICO',                           description: 'ácido acrílico'},
                {title: 'ÁCIDO ASCÓRBICO',                          description: 'ácido ascórbico'},
                {title: 'ÁCIDO BÓRICO',                             description: 'ácido bórico'},
                {title: 'ÁCIDO CÁPRICO',                            description: 'ácido cáprico'},
                {title: 'ÁCIDO CAPRÍLICO',                          description: 'ácido caprílico'},
                {title: 'ÁCIDO CÍTRICO ANÍDRO',                     description: 'ácido cítrico anídro'},
                {title: 'ÁCIDO CÍTRICO ANÍDRO FINO',                description: 'ácido cítrico anídro fino'},
                {title: 'ÁCIDO CLORÍDRICO 33%',                     description: 'ácido clorídrico 33%'},
                {title: 'ÁCIDO CLORÍDRICO 37%',                     description: 'ácido clorídrico 37%'},
                {title: 'ACIDO FLUORÍDRICO 71%',                    description: 'acido fluorídrico 71%'},
                {title: 'ÁCIDO FOSFÓRICO 51%',                      description: 'ácido fosfórico 51%'},
                {title: 'ÁCIDO FOSFÓRICO 54%',                      description: 'ácido fosfórico 54%'},
                {title: 'ÁCIDO FOSFÓRICO 85%',                      description: 'ácido fosfórico 85%'},
                {title: 'ÁCIDO FOSFOROSO',                          description: 'ácido fosforoso'},
                {title: 'ÁCIDO FUMÁRICO',                           description: 'ácido fumárico'},
                {title: 'ÁCIDO LAURICO',                            description: 'ácido laurico'},
                {title: 'ÁCIDO NÍTRICO 33%',                        description: 'ácido nítrico 33%'},
                {title: 'ÁCIDO OXÁLICO ANIDRO',                     description: 'ácido oxálico anidro'},
                {title: 'ÁCIDO OXÁLICO DIIDRATADO',                 description: 'ácido oxálico diidratado'},
                {title: 'ÁCIDO SULFÔNICO 90%',                      description: 'ácido sulfônico 90%'},
                {title: 'ÁCIDO SULFÔNICO 96%',                      description: 'ácido sulfônico 96%'},
                {title: 'ÁCIDO SULFÚRICO 98%',                      description: 'ácido sulfúrico 98%'},
                {title: 'ÁCIDO TRICLOROISOCIANÚRICO PÓ',            description: 'ácido tricloroisocianúrico pó'},
                {title: 'ÁCIDO TRICLOROISOCIANÚRICO TABLETES',      description: 'ácido tricloroisocianúrico tabletes'},
                {title: 'ÁLCOOL CETO ESTEARÍLICO 30/70',            description: 'álcool ceto estearílico 30/70'},
                {title: 'ÁLCOOL CETO ESTEARÍLICO 70/30',            description: 'álcool ceto estearílico 70/30'},
                {title: 'ÁLCOOL ETÍLICO 96%',                       description: 'álcool etílico 96%'},
                {title: 'ÁLCOOL ETÍLICO 99%',                       description: 'álcool etílico 99%'},
                {title: 'ÁLCOOL ISOBUTÍLICO (ISOBUTANOL)',          description: 'álcool isobutílico (isobutanol)'},
                {title: 'ÁLCOOL NEUTRO',                            description: 'álcool neutro'},
                {title: 'AMIDA 60',                                 description: 'amida 60'},
                {title: 'AMIDA 80',                                 description: 'amida 80'},
                {title: 'AMIDA 90',                                 description: 'amida 90'},
                {title: 'ANIDRIDO FTÁLICO',                         description: 'anidrido ftálico'},
                {title: 'ANIDRIDO MALEICO',                         description: 'anidrido maleico'},
                {title: 'BARRILHA LEVE',                            description: 'barrilha leve'},
                {title: 'BASE AMACIANTE PASTOSA',                   description: 'base amaciante pastosa'},
                {title: 'BENZOATO DE SÓDIO GRANULADO',              description: 'benzoato de sódio granulado'},
                {title: 'BICARBONATO DE AMÔNIO',                    description: 'bicarbonato de amônio'},
                {title: 'BICARBONATO DE SÓDIO',                     description: 'bicarbonato de sódio'},
                {title: 'BICARBONATO DE SÓDIO EXTRA FINO',          description: 'bicarbonato de sódio extra fino'},
                {title: 'BÓRAX DECAHIDRATADO',                      description: 'bórax decahidratado'},
                {title: 'BRANCOL',                                  description: 'brancol'},
                {title: 'BUTILGLICOL',                              description: 'butilglicol'},
                {title: 'CACAU EM PÓ ALCALINO',                     description: 'cacau em pó alcalino'},
                {title: 'CACAU EM PÓ LECITINADO',                   description: 'cacau em pó lecitinado'},
                {title: 'CACAU EM PÓ NATURAL',                      description: 'cacau em pó natural'},
                {title: 'CAFEÍNA ANÍDRA',                           description: 'cafeína anídra'},
                {title: 'CARBONATO DE CÁLCIO PRECIPITADO LEVE',     description: 'carbonato de cálcio precipitado leve'},
                {title: 'CARBONATO DE CÁLCIO PRECIPITADO MÉDIO',    description: 'carbonato de cálcio precipitado médio'},
                {title: 'CARBONATO DE POTÁSSIO',                    description: 'carbonato de potássio'},
                {title: 'CARBOPOL',                                 description: 'carbopol'},
                {title: 'CITRATO DE SÓDIO DIHIDRATADO',             description: 'citrato de sódio dihidratado'},
                {title: 'CLORETO DE CÁLCIO DIHIDRATADO',            description: 'cloreto de cálcio dihidratado'},
                {title: 'CLORETO DE SÓDIO MOÍDO',                   description: 'cloreto de sódio moído'},
                {title: 'DOP',                                      description: 'dop'},
                {title: 'ENXÔFRE ESCAMADO',                         description: 'enxôfre escamado'},
                {title: 'ENXÔFRE GRANULADO',                        description: 'enxôfre granulado'},
                {title: 'ENXÔFRE LENTILHADO',                       description: 'enxôfre lentilhado'},
                {title: 'ENXÔFRE PECUÁRIO',                         description: 'enxôfre pecuário'},
                {title: 'ENXÔFRE VENTILADO',                        description: 'enxôfre ventilado'},
                {title: 'FORMOL ESTABILIZADO 37%',                  description: 'formol estabilizado 37%'},
                {title: 'FORMOL INIBIDO 37%',                       description: 'formol inibido 37%'},
                {title: 'GLICERINA BRANCA BIDESTILADA',             description: 'glicerina branca bidestilada'},
                {title: 'HIDROSSULFITO DE SÓDIO',                   description: 'hidrossulfito de sódio'},
                {title: 'HIDRÓXIDO DE AMÔNIO',                      description: 'hidróxido de amônio'},
                {title: 'HIPOCLORITO DE SÓDIO 12%',                 description: 'hipoclorito de sódio 12%'},
                {title: 'LAURIL ÉTER SULFATO DE SÓDIO 27%',         description: 'lauril éter sulfato de sódio 27%'},
                {title: 'LAURIL ÉTER SULFATO DE SÓDIO 70%',         description: 'lauril éter sulfato de sódio 70%'},
                {title: 'LECITINA DE SOJA',                         description: 'lecitina de soja'},
                {title: 'MANTEIGA DE KARITÉ',                       description: 'manteiga de karité'},
                {title: 'METABISSULFITO DE SÓDIO',                  description: 'metabissulfito de sódio'},
                {title: 'METASSILICATO DE SÓDIO',                   description: 'metassilicato de sódio'},
                {title: 'MIRISTATO DE ISOPROPILA',                  description: 'miristato de isopropila'},
                {title: 'NITRATO DE SÓDIO',                         description: 'nitrato de sódio'},
                {title: 'NITRITO DE SÓDIO',                         description: 'nitrito de sódio'},
                {title: 'NONILFENOL ETOXILADO 95%',                 description: 'nonilfenol etoxilado 95%'},
                {title: 'OCTANOL',                                  description: 'octanol'},
                {title: 'ÓLEO DE CITRONELA',                        description: 'óleo de citronela'},
                {title: 'ÓLEO DE EUCALÍPTO',                        description: 'óleo de eucalípto'},
                {title: 'ÓLEO DE MACADÂMIA',                        description: 'óleo de macadâmia'},
                {title: 'ÓLEO DE PINHO 65%',                        description: 'óleo de pinho 65%'},
                {title: 'PERMANGANATO DE POTÁSSIO',                 description: 'permanganato de potássio'},
                {title: 'PERÓXIDO DE HIDROGÊNIO 200 VOL (50%)',     description: 'peróxido de hidrogênio 200 vol (50%)'},
                {title: 'POLÍMERO ANIÔNICO',                        description: 'polímero aniônico'},
                {title: 'PROPILENO GLICOL',                         description: 'propileno glicol'},
                {title: 'PROPIONATO DE CALCIO',                     description: 'propionato de calcio'},
                {title: 'SEBO BOVINO CLARIFICADO',                  description: 'sebo bovino clarificado'},
                {title: 'SODA CÁUSTICA ESCAMAS 98%',                description: 'soda cáustica escamas 98%'},
                {title: 'SODA CÁUSTICA LÍQUIDA',                    description: 'soda cáustica líquida'},
                {title: 'SORBATO DE POTÁSSIO',                      description: 'sorbato de potássio'},
                {title: 'SULFATO DE ALUMÍNO ISENTO DE FERRO PÓ',    description: 'sulfato de alumíno isento de ferro pó'},
                {title: 'SULFATO DE AMÔNIO BRANCO',                 description: 'sulfato de amônio branco'},
                {title: 'SULFATO DE SÓDIO ANÍDRO',                  description: 'sulfato de sódio anídro'},
                {title: 'SULFITO DE SÓDIO',                         description: 'sulfito de sódio'},
                {title: 'TRIPOLIFOSFATO DE SÓDIO',                  description: 'tripolifosfato de sódio'},
                {title: 'URÉIA AGRÍCOLA',                           description: 'uréia agrícola'},
                {title: 'URÉIA PECUÁRIA',                           description: 'uréia pecuária'},
                {title: 'URÉIA TÉCNICA',                            description: 'uréia técnica'},
                {title: 'VASELINA LIQUIDA',                         description: 'vaselina liquida'},
                {title: 'VASELINA SÓLIDA',                          description: 'vaselina sólida'}
              ])



Product.find(:all).each {|i| i.create_fulltext_record}
