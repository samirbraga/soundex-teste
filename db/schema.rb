# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130531160008) do

  create_table "fulltext_rows", :force => true do |t|
    t.string  "fulltextable_type", :limit => 50, :null => false
    t.integer "fulltextable_id",                 :null => false
    t.text    "value",                           :null => false
    t.integer "parent_id"
  end

  add_index "fulltext_rows", ["fulltextable_type", "fulltextable_id"], :name => "index_fulltext_rows_on_fulltextable_type_and_fulltextable_id", :unique => true
  add_index "fulltext_rows", ["parent_id"], :name => "index_fulltext_rows_on_parent_id"
  add_index "fulltext_rows", ["value"], :name => "fulltext_index"

  create_table "products", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.string   "title_phonetized"
    t.text     "description_phonetized"
  end

end
