class ProductsController < ApplicationController
  # GET /products
  # GET /products.json
  def index

    @kw = params[:q]
    if @kw.blank?
      @products = @products2 = @products3 = @products4 = Product.all
    else
      @kw_phonetized = @kw.to_sound

      kw_like_query = "%#{@kw}%"
      kw_regex_query = "[[:<:]]#{@kw_phonetized}[[:>:]]"


      # Alternativa #1: qualquer parte da palavra
      @products = Product.where("title LIKE ? OR description LIKE ? OR title_phonetized LIKE ? OR description_phonetized LIKE ?", kw_like_query, kw_like_query, kw_like_query, kw_like_query)

      # Alternativa #2: palavras inteiras
      @products2 = Product.where("title LIKE ? OR description LIKE ? OR title_phonetized REGEXP ? = 1 OR description_phonetized REGEXP ? = 1", kw_like_query, kw_like_query, kw_regex_query, kw_regex_query)

      # Alternativa #3: com acts_as_indexed
      @products3 = (Product.find_with_index(@kw) + Product.find_with_index(@kw_phonetized)).uniq

      # Alternativa #4: com acts_as_fulltextable
      @products4 = (Product.find_fulltext(@kw) + Product.find_fulltext(@kw_phonetized)).uniq.delete_if {|x| x == nil}

    end

    @list = Product.pluck(:title).join(" ").split.uniq.sort

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @products }
    end
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @product = Product.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @product }
    end
  end

  # GET /products/new
  # GET /products/new.json
  def new
    @product = Product.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @product }
    end
  end

  # GET /products/1/edit
  def edit
    @product = Product.find(params[:id])
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(params[:product])

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render json: @product, status: :created, location: @product }
      else
        format.html { render action: "new" }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /products/1
  # PUT /products/1.json
  def update
    @product = Product.find(params[:id])

    respond_to do |format|
      if @product.update_attributes(params[:product])
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product = Product.find(params[:id])
    @product.destroy

    respond_to do |format|
      format.html { redirect_to products_url }
      format.json { head :no_content }
    end
  end
end
