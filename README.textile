h1. Soundexo

bq. git clone git@bitbucket.org:samirbraga/soundex-teste.git
    cd soundex-teste
    rake db:setup
    rake db:migrate
    rake db:seed
    rails s

Acessar http://0.0.0.0:3000/products

h2. Orientação

p. Testei 2 versões de busca

p. A primeira busca por qualquer trecho da palavra fonetizada, o que retorna dados bem estranhos com relação à busca,
p. A segunda corrige isso, buscando por palavras inteiras (delimitadas por espaço ou começo/final da string)
